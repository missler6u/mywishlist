<?php
/**
 * Created by PhpStorm.
 * User: missler6u
 * Date: 09/01/2019
 * Time: 11:47
 */

namespace mywishlist\controls;

use mywishlist\utils\SlimSingleton;
use mywishlist\vues\VueUtil;

class UtilController {
    private $vue;

    public function __construct() {
        $this->vue = new VueUtil();
        $this->app = SlimSingleton::getInstance();
        $this->url = $this->app->request->getRootUri();
    }

    public function connexion() {
        if(isset($_POST['connexion'])) {
            if(!empty($_POST['pass']) && !empty($_POST['login'])) {
                $membres = \mywishlist\models\Membre::get();
                $existe = false;
                foreach ($membres as $membre) {
                    if ($membre->login == $_POST['login']) {
                        $existe = true;
                        $idMembre = $membre->idMembre;
                    }
                }
                if($existe) {
                    $existe = false;
                    foreach ($membres as $membre) {
                        if (password_verify($_POST['pass'], $membre->pass)) $existe = true;
                    }
                    if($existe) {
                        $_SESSION['login'] = $idMembre;
                        header('Location: '.$this->url);
                        exit();
                    }
                    else $this->vue->fauxPass();
                }
                else $this->vue->pseudoInconnu();
            }
            else $this->vue->champVide();
        }

        $this->vue->connexion();
    }

    public function inscription() {
        if(isset($_POST['inscription'])) {
            if(!empty($_POST['pass']) && !empty($_POST['login'])) {
                $membres = \mywishlist\models\Membre::get();
                $nbVal = count($membres);
                $existe = false;
                foreach ($membres as $membre) {
                    if ($membre->login == $_POST['login']) $existe = true;
                }
                if (!$existe) {
                    $hash = password_hash($_POST['pass'], PASSWORD_DEFAULT);
                    $m = new \mywishlist\models\Membre();
                    $m->idMembre = $nbVal;
                    $m->login = $_POST['login'];
                    $m->pass = $hash;
                    $m->save();
                    $this->vue->compteCreee();
                } else $this->vue->pseudoConnu();
            }
            else $this->vue->champVide();
        }

        $this->vue->inscription();
    }

    public function modifier() {
        if(isset($_POST['modifPass'])) {
            if(!empty($_POST['oldPass']) && !empty($_POST['newPass'])) {
                $membre = \mywishlist\models\Membre::where('login', '=', $_SESSION['login'])
                                                    ->first();
                if(password_verify($_POST['oldPass'], $membre->pass)) {
                    $hash = password_hash($_POST['newPass'], PASSWORD_DEFAULT);
                    $membre->pass = $hash;
                    $membre->save();
                    $this->deconnexion();
                }
                else $this->vue->fauxPass();
            }
            else $this->vue->champVide();
        }
        $this->vue->modifier();
    }

    public function deconnexion() {
        unset($_SESSION['login']);
        header('Location: '.$this->url);
        exit();
    }
}