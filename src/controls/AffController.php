<?php
/**
 * Created by PhpStorm.
 * User: missler6u
 * Date: 09/01/2019
 * Time: 11:47
 */

namespace mywishlist\controls;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Message;
use mywishlist\models\Reservation;
use mywishlist\utils\SlimSingleton;
use mywishlist\vues\VueItem;
use mywishlist\vues\VueListe;


class AffController {
    private $vueListe, $vueItem;

    public function __construct() {
        $this->vueListe = new VueListe();
        $this->vueItem = new VueItem();
        $this->app = SlimSingleton::getInstance();
        $this->url = $this->app->request->getRootUri();
    }

    public function affListe($token) {
        $liste = Liste::where('token', '=', $token)
            ->first();

        $this->vueListe->affListe($liste);
    }

    public function affItem($id) {
        $item = Item::where('id', '=', $id)
            ->first();

        $liste = $item->liste;

        if(isset($_GET['suppItem']) && $_SESSION['login'] == $liste->user_id) {
            $liste = $item->liste;
            $token = $liste->token;
            $item->delete();
            header('Location: '.$this->url.'/liste/afficher/'.$token);
            exit();
        }

        if(is_null($liste)) {
            $this->vueItem->nonliste();
        }
        else $this->vueItem->affItem($item);
    }

    public function modifListe($token) {
        $liste = Liste::where('token', '=', $token)
            ->first();
        if($_SESSION['login'] == $liste->user_id) {
            $this->vueListe->modifListe($liste);

            if (isset($_GET['modifListe'])) {
                    $today = date("Y-m-d");
                    if (strtotime($today) >= strtotime($_GET['nvExp']) && !empty($_GET['nvExp'])) {
                        $this->vueListe->mauvaiseDate();
                    } else {
                        if (!empty($_GET['nvTitre'])) $liste->titre = filter_var($_GET['nvTitre'], FILTER_SANITIZE_STRING);
                        if (!empty($_GET['nvDesc'])) $liste->description = filter_var($_GET['nvDesc'], FILTER_SANITIZE_STRING);
                        if (!empty($_GET['nvExp'])) $liste->expiration = filter_var($_GET['nvExp'], FILTER_SANITIZE_STRING);
                        $liste->status = $_GET['status'];
                        $liste->save();
                        $this->vueListe->listeModifiee();
                    }
                }


            $this->vueItem->ajtItem();

            if (isset($_GET['ajtItem'])) {
                if (!empty($_GET['itemNom']) && !empty($_GET['itemDescr']) && !empty($_GET['itemTarif'])) {
                    $nom = filter_var($_GET['itemNom'], FILTER_SANITIZE_STRING);
                    $descr = filter_var($_GET['itemDescr'], FILTER_SANITIZE_STRING);
                    $items = Item::all();
                    $id = 0;
                    foreach ($items as $item) {
                        if ($item->id > $id) $id = $item->id;
                    }
                    $id = $id + 1;
                    $item = new Item();
                    $item->id = $id;
                    $item->liste_id = $liste->no;
                    $item->nom = $nom;
                    $item->descr = $descr;
                    if (!empty($_GET['itemLien'])) $item->url = filter_var($_GET['itemLien'], FILTER_SANITIZE_STRING);
                    $item->tarif = $_GET['itemTarif'];
                    $item->save();
                    $this->vueItem->itemCree();
                } else {
                    $this->vueItem->champVide();
                }
            }

            $this->vueListe->suppListe();
        }

        if(isset($_GET['suppListe']) && $_SESSION['login'] == $liste->user_id) {
            $messages = $liste->messages;
            foreach ($messages as $message) $message->delete();

            $items = $liste->items;
            foreach ($items as $item) {
                $reservation = $item->reservation;
                if(!is_null($reservation))$reservation->delete();
                $item->delete();
            }



            $liste->delete();
            header('Location: '.$this->url);
            exit();
        }
    }

    public function modifItem($id)
    {
        $reserv = Reservation::where('idItem', '=', $id)->first();
        if(is_null($reserv)) {
            $item = Item::where('id', '=', $id)
                ->first();
            $liste = $item->liste;
            if ($_SESSION['login'] == $liste->user_id) {
                $this->vueItem->modifItem($item);

                if (isset($_GET['modifItem'])) {
                    if (!empty($_GET['nvNom']) || !empty($_GET['nvDescr']) || !empty($_GET['nvTarif']) || !empty($_GET['nvLien'])) {
                        if (!empty($_GET['nvNom'])) $item->nom = filter_var($_GET['nvNom'], FILTER_SANITIZE_STRING);
                        if (!empty($_GET['nvDescr'])) $item->descr = filter_var($_GET['nvDescr'], FILTER_SANITIZE_STRING);
                        if (!empty($_GET['nvTarif'])) $item->tarif = filter_var($_GET['nvTarif'], FILTER_SANITIZE_STRING);
                        $item->save();
                        $this->vueItem->itemModifiee();
                    } else {
                        $this->vueItem->champsVide();
                    }
                }
            }
        }
    }

    function reserverItem($idItem, $token){
        $item = Item::where(['id' => $idItem])->first();
        $nom =  filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
        $message =  filter_var($_POST['message'],FILTER_SANITIZE_STRING);

        $reserv = new Reservation();
        $reserv->idItem = $idItem;
        $reserv->nom = $nom;
        $reserv->message = $message;
        $reserv->save();

        $_SESSION['nom'] = $nom;

        header('Location: '.$this->url.'/item/afficher/'.$idItem.'/'.$token);
        exit();
    }

    function nvMessage($token){
        $liste = Liste::where('token', '=', $token)
            ->first();
        $msg = filter_var($_POST['msg'],FILTER_SANITIZE_STRING);

        $messages = Message::get();
        $id = count($messages)+1;

        $message = new Message();
        $message->idMessage = $id;
        $message->idListe = $liste->no;
        $message->message = $msg;
        $message->idMembre = $_SESSION['login'];
        $message->save();

        header('Location: '.$this->url.'/liste/afficher/'.$token);
        exit();
    }
}