<?php
/**
 * Created by PhpStorm.
 * User: missler6u
 * Date: 09/01/2019
 * Time: 11:47
 */

namespace mywishlist\controls;

use mywishlist\models\Liste;
use mywishlist\vues\VueListe;
use mywishlist\vues\VueNavBar;
use mywishlist\vues\VueUtil;

class MainController {
    private $vueNB, $vueListe, $vueUtil;

    public function __construct() {
        $this->vueNB = new VueNavBar();
        $this->vueListe = new VueListe();
        $this->vueUtil = new VueUtil();
    }

    public function navBar() {
        $this->index();
        $this->util();
    }

    public function util() {
        if(!isset($_SESSION['login'])) {
            $this->vueNB->util();
        }
        else {
            $this->vueNB->modifUtil();
            $this->vueNB->deconnexion();
        }
    }

    public function index() {
         $this->vueNB->index();
    }

    public function page() {
            $this->affListes();
            $this->nvListe();
    }

    public function affListes() {
        $listesPub = Liste::where('status', '=', 'publique')->get();
        $this->vueListe->affListesPub($listesPub);
        if(isset($_SESSION['login'])){
            $listes = Liste::where('status', '=', 'privee')->where('user_id', '=', $_SESSION['login'])->orderBy('expiration')->get();
            $this->vueListe->affListesPri($listes);
        }
    }

    public function nvListe() {
        if(isset($_SESSION['login'])) {
            $this->vueListe->NvListe();
            if (isset($_GET['nvListe'])) {
                if (!empty($_GET['titreListe']) && !empty($_GET['descListe']) && !empty($_GET['dateListe'])) {
                    $today = date("Y-m-d");

                    if (strtotime($today) >= strtotime($_GET['dateListe'])) {
                        $this->vueListe->mauvaiseDate();
                    } else {
                        $titre = filter_var($_GET['titreListe'], FILTER_SANITIZE_STRING);
                        $desc = filter_var($_GET['descListe'], FILTER_SANITIZE_STRING);
                        $exp = filter_var($_GET['dateListe'], FILTER_SANITIZE_STRING);
                        $listes = Liste::get();
                        $no = 0;
                        foreach ($listes as $liste) {
                            if ($liste->no > $no) $no = $liste->no;
                        }
                        $no = $no + 1;
                        $token = "nosecure" . $no;
                        $l = new Liste();
                        $l->no = $no;
                        $l->titre = $titre;
                        $l->description = $desc;
                        $l->user_id = $_SESSION['login'];
                        $l->expiration = $exp;
                        $l->token = $token;
                        $l->status = $_GET['status'];
                        $l->save();
                        $this->vueListe->listeCreee();
                    }
                } else {
                    $this->vueListe->champVide();
                }
            }
        }
    }
}