<?php
/**
 * Created by PhpStorm.
 * User: missler6u
 * Date: 09/01/2019
 * Time: 11:47
 */

namespace mywishlist\models;

class Membre extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'membre';
    protected $primaryKey = 'idMembre';
    public $timestamps = false;

    public function item() {
        return $this->hasOne('mywishlist\models\Message', 'idMessage') ;
    }
}