<?php

namespace mywishlist\models;

class Message extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'message';
    protected $primaryKey = 'idMessage';
    public $timestamps = false;

    public function liste() {
        return $this->belongsTo('mywishlist\models\Liste', 'idListe') ;
    }

    public function membre() {
        return $this->belongsTo('mywishlist\models\Membre', 'idMembre') ;
    }
}
