<?php
/**
 * Created by PhpStorm.
 * User: missler6u
 * Date: 09/01/2019
 * Time: 11:47
 */

namespace mywishlist\models;

class Reservation extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'reservation';
    protected $primaryKey = 'idItem';
    public $timestamps = false;

    public function item() {
        return $this->belongsTo('mywishlist\models\Item', 'idItem') ;
    }
}