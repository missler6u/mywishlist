<?php
namespace mywishlist\utils;

use Slim\Slim;

class SlimSingleton {
    private static $instance;

    private function __construct() {}

    public static function getInstance() {
        if(!isset(self::$instance)) {
            self::$instance = new Slim();
        }
        return self::$instance;
    }
}