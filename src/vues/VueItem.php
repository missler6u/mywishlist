<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 09/01/2019
 * Time: 16:54
 */

namespace mywishlist\vues;

use mywishlist\models\Item;
use mywishlist\models\Reservation;
use mywishlist\utils\SlimSingleton;

class VueItem
{
    public function __construct()
    {
        $this->app = SlimSingleton::getInstance();
        $this->url = $this->app->request->getRootUri();
    }

    public function affItem(Item $item) {
        $liste = $item->liste;
        $reserv = Reservation::where('idITem', '=', $item->id)->first();
        $html = '
       <h2>Nom de l\'item : '.$item->nom.'</h2>';
       if(isset($_SESSION['login'])) if($_SESSION['login'] == $liste->user_id && is_null($reserv)) $html = $html.'<a href="'.$this->url.'/item/modif/'.$item->id.'/'.$liste->token.'">Modifier l\'item</a>';
       $html = $html.'<h5>id : '.$item->id.'<br>
       liste(id): '.$item->liste_id.'</h5>
       <h3>Description :</h3>
       <p>'.$item->descr.'</p>
       <p>tarif : '.$item->tarif.'</p>';


       if(!is_null($item->img)) $html = $html.'<img src="'.$this->url.'/web/img/'.$item->img.'" height="10%" width="auto"></p>';
       if(!(empty($item->url) || is_null($item->url))) $html = $html.'<p>lien page externe : '.$item->url.'</p>';

        $today = date("Y-m-d");

        if((strtotime($today) < strtotime($liste->expiration) && (isset($_SESSION['login']) && $_SESSION['login'] != $liste->user_id) || (strtotime($today) < strtotime($liste->expiration) && !isset($_SESSION['login'])))) if (is_null($reserv)) {
            if(isset($_SESSION['nom'])) {
                $nom=$_SESSION['nom'];
            }
            else $nom = null;
                $html = $html . '<form action=\''.$this->url.'/item/reserver/' . $item->id . '/' . $liste->token . '\' method=\'POST\'>
            <input name=\'nom\' type=\'text\' placeholder=\'Votre nom\' value="'.$nom.'" required/>
            <input name=\'message\'type="text" placeholder=\'Votre message\'>
            <input type=\'submit\' value=\'Réserver\'/>
        </form>';
            }
        if (!is_null($reserv))
        {
            $html = $html . 'Item Réservé ';
            if ((isset($_SESSION['login']) && $_SESSION['login'] != $liste->user_id) || strtotime($today) >= strtotime($liste->expiration) || !isset($_SESSION['login'])) $html = $html . ': ' . $reserv->nom;
            if(strtotime($today) >= strtotime($liste->expiration)) $html = $html.'<p>Message : '.$reserv->message.'</p>';
        }


        if(isset($_SESSION['login'])) {
            if($_SESSION['login'] == $liste->user_id && is_null($reserv)) $html = $html.'
       <form method = "get" action="">
            <input type = "submit" name = "suppItem" value = "Supprimer l\'item">
       </form>';
        };
        $html = $html.'<p><a href="'.$this->url.'/liste/afficher/'.$liste->token.'">Retourner à la liste de souhaits</a></p>';

        echo $html;
    }

    public function nonliste() {
        $html = <<<END
       <p>Erreur : cet item ne peut être affiché car il n'appartient à aucune liste de souhaits validé par son constructeur.</p>
END;
        echo $html;
    }

    public function champVide() {
        $html = <<<END
       <p>Veuillez remplir tous les champs</p>
END;
        echo $html;
    }

    public function itemModifiee() {
        $html = <<<END
       <p>Votre item à bien été modifié</p>
END;
        echo $html;
    }

    public function ajtItem() {
        $html = <<<END
       <h3>Ajouter Item :</h3>
       
       <form method = "get" action="">
            <input type="text" name="itemNom" placeholder="nom de l'item">
            <input type="text"  name="itemDescr" placeholder="descriptiond de l'item">
            <input type="number" step="0.01" name="itemTarif" placeholder="prix de l'item">
            <input type="text"  name="itemLien" placeholder="lien page externe (optionnel)">
            <input type = "submit" name = "ajtItem" value = "Ajouter">
       </form>
END;
        echo $html;
    }

    public function itemCree() {
        $html = <<<END
       <p>Votre item à bien été créé</p>
END;
        echo $html;
    }

    public function champsVide() {
        $html = <<<END
       <p>Veuillez remplir au moins 1 champ</p>
END;
        echo $html;
    }

    public function modifItem(Item $item) {
        $liste = $item->liste;
        $html = <<<END
        <p><a href="$this->url/item/afficher/$item->id/$liste->token">Retourner à l'item</a></p>    
       <h2>Nom de l'item : $item->nom</h2>
       <h5>id : $item->id<br>
       liste(id) : $item->liste_id</h5>
       <h3>Description :</h3>
       <p>$item->desc</p>
       <p>Tarif : $item->tarif</p>

       <h3>Modification des informations générales :</h3>

       <p>Entrer dans les champs ce que vous voulez modifier :</p>

       <form method = "get" action="">
            <input type="text" name="nvNom" placeholder="nouveau nom">
            <input type="text"  name="nvDescr" placeholder="nouvelle description">
            <input type="number" step="0.01"  name="nvTarif" placeholder="nouveau tarif">
            <input type="text"  name="nvLien" placeholder="nouveau lien page externe">
            <input type = "submit" name = "modifItem" value = "Modifier">
       </form>   
       
       
END;
        echo $html;
    }
}