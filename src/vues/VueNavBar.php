<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 09/01/2019
 * Time: 16:54
 */

namespace mywishlist\vues;


use mywishlist\models\Membre;use mywishlist\utils\SlimSingleton;

class VueNavBar
{

    public function __construct()
    {
        $this->app = SlimSingleton::getInstance();
        $this->url = $this->app->request->getRootUri();
    }

    public function index() {

        if(isset($_SESSION['login'])) {
            $id = $_SESSION['login'];
            $membre = Membre::where('idMembre', '=', $_SESSION['login'])->first();
            $login = $membre->login;
        }

        if(isset($_SESSION['login'])) $html = <<<END
       <form method = "post" action="$this->url">
            <input type = "submit" name = "Accueil" value = "Accueil">
            Connecté sous le compte : $login
        </form>
END;
        else $html = <<<END
       <form method = "post" action="$this->url">
            <input type = "submit" name = "Accueil" value = "Accueil">
        </form>
END;
        echo $html;
    }

    public function util() {
        $html = <<<END
       <form method = "post" action="$this->url/connexion">
            <input type = "submit" name = "connexion" value = "Se connecter">
        </form>

        <form method = "post" action="$this->url/inscription">
            <input type = "submit" name = "inscription" value = "S'inscrire">
        </form>
END;
        echo $html;
    }

    public function modifUtil() {
        $html = <<<END
       <form method = "post" action="$this->url/modifierCompte">
            <input type = "submit" name = "modifier" value = "Modifier son compte">
        </form>
END;
        echo $html;
    }

    public function deconnexion() {
        $html = <<<END
       <form method = "post" action ="$this->url/deconnexion">
            <input type = "submit" name = "deconnexion" value = "Se deconnecter">
        </form>
END;
        echo $html;
    }

}