<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 09/01/2019
 * Time: 16:54
 */

namespace mywishlist\vues;

use mywishlist\models\Liste;
use mywishlist\models\Reservation;
use mywishlist\utils\SlimSingleton;

class VueListe
{
    public function __construct()
    {
        $this->app = SlimSingleton::getInstance();
        $this->url = $this->app->request->getRootUri();
    }

    public function affListe(Liste $l) {
        $today = date("Y-m-d");
        $html = '
       <h2>Titre de la liste : '.$l->titre;
        if(strtotime($today) >= strtotime($l->expiration)) $html = $html.' (Expirée)</h2>';
       if(isset($_SESSION['login']) )if($_SESSION['login'] == $l->user_id) $html = $html.'</h2><a href="'.$this->url.'/liste/modif/'.$l->token.'">Modifier la liste</a>';
       $html = $html.'<h5>no : '.$l->no.'<br>
       token : '.$l->token.'</h5>
       <h3>Description :</h3>
       <p>'.$l->description.'</p>
       <p>Expiration : '.$l->expiration.'</p>
       
       <h2>Liste des items de la liste de souhaits :</h2>';
        echo $html;

        $this->listeItems($l);
        if(isset($_SESSION['login']) && ($_SESSION['login'] != $l->user_id) || (strtotime($today) >= strtotime($l->expiration))) $this->listeMessage($l);
        if(isset($_SESSION['login'])) if($_SESSION['login'] != $l->user_id) $this->nvMessage($l);
        if(isset($_SESSION['login'])) if($_SESSION['login'] == $l->user_id) $this->partage($l);
    }

    public function listeItems(Liste $l) {
        $items = $l->items;
        if(count($items) >0) {
            $html = '<ul>';
            foreach ($items as $item) {
                $today = date("Y-m-d");
                $html = $html . '<li><p><a href="'.$this->url.'/item/afficher/' . $item->id . '/'.$l->token.'">' . $item->nom . '</a>';
                $reserv = Reservation::where('idITem', '=', $item->id)->first();

                    if (!is_null($reserv))
                    {
                        $html = $html . '  Réservé ';
                        if ((isset($_SESSION['login']) && $_SESSION['login'] != $l->user_id) || strtotime($today) >= strtotime($l->expiration) || !isset($_SESSION['login'])) $html = $html . ': ' . $reserv->nom;
                     }
                    if (is_null($reserv)) $html = $html . '  Non Réservé';

                if (!is_null($item->img)) $html = $html . '</li> <img src="'.$this->url.'/web/img/' . $item->img . '" height="10%" width="auto"></p><br>';
            }
            $html = $html . '</ul>';
        }
        else $html = 'Cette liste ne contient aucun items';
        echo $html;
    }

    public function listeMessage(Liste $l) {
        $messages = $l->messages;

        $html = '<h2>Liste des messages de la liste de souhaits :</h2>';
        if(count($messages) >0) {

            $html = $html. '<ul>';
            foreach ($messages as $message) {
                $membre = $message->membre;
                $html = $html . '<li>'. $message->message . ' ('.$membre->login.')</li>';
            }
            $html = $html . '</ul>';
        }
        else $html = $html.'Cette liste ne contient aucun messages';

        echo $html;
    }

    public function partage(Liste $l) {
        $app = \mywishlist\utils\SlimSingleton::getInstance();
        $url = $app->request->getHost();

            $html = <<<END
       <form method = "get" action="">
            <p>Partager :
            <input type = "text" value = "$url$this->url/liste/afficher/$l->token"></p>
       </form>    
END;

        echo $html;
    }


    public function nvMessage(Liste $l) {
        $html = <<<END
        <h3>Nouveau message :</h3>
        
       <form method = "post" action="$this->url/message/new/$l->token">
            <input type="text" name="msg" placeholder="votre message">
            <input type = "submit" name = "envoyerMsg" value = "Envoyer">
       </form>    
END;
        echo $html;
    }
    
    public function affListesPri($listes) {
        $html = '<h2>Listes de souhaits privées :</h2><ul>';
        foreach($listes as $l) {
            $html = $html.'<li><a href="liste/afficher/'.$l->token.'">'.$l->titre.'</a></li>';
        }
        $html = $html.'</ul>';

        echo $html;
    }

    public function affListesPub($listesPub) {
        $html = '<h2>Listes de souhaits publiques :</h2><ul>';
        $today = date("Y-m-d");

        foreach($listesPub as $l) {
            if(strtotime($today) < strtotime($l->expiration))
            {
                $html = $html.'<li><a href="liste/afficher/'.$l->token.'">'.$l->titre.'</a></li>';
            }
        }
        $html = $html.'</ul>';

        echo $html;
    }

    public function nvListe() {
        $html = '
        <h3>Nouvelle liste :</h3>
       <form method = "get" action="">
            <input type="text" name="titreListe" placeholder="titre de la liste">
            <input type="text" name="descListe" placeholder="description de la liste">
            <input type="date" name="dateListe">';

            if(isset($_SESSION['login'])) {
                $html = $html.'<input type="radio" name="status" value="privee" checked/>Privée
               <input type="radio" name="status" value="publique"/>Publique';
            }
            else {
               $html = $html.'<input type="radio" name="status" value="publique" checked/>Publique';
            }

            $html = $html.'<input type = "submit" name = "nvListe" value = "Créer la liste">
        </form>';

        echo $html;
    }

    public function champVide() {
        $html = <<<END
       <p>Veuillez remplir tous les champs</p>
END;
        echo $html;
    }

    public function champsVide() {
        $html = <<<END
       <p>Veuillez remplir au moins 1 champ</p>
END;
        echo $html;
    }

    public function listeCreee() {
        $html = <<<END
       <p>Votre liste à bien été créée</p>
END;
        echo $html;
    }

    public function listeModifiee() {
        $html = <<<END
       <p>Votre liste à bien été modifiée</p>
END;
        echo $html;
    }

    public function mauvaiseDate() {
        $html = <<<END
       <p>Erreur : la date  doit être ultérieure à la date actuelle</p>
END;
        echo $html;
    }

    public function modifListe(Liste $l) {
        $html = <<<END
        <p><a href="$this->url/liste/afficher/$l->token">Retourner à la liste de souhaits</a></p>
       <h2>Titre de la liste : $l->titre</h2>
       <h5>no : $l->no<br>
       token : $l->token</h5>
       <h3>Description :</h3>
       <p>$l->description</p>
       <p>Expiration : $l->expiration</p>

       <h3>Modification des informations générales :</h3>

       <p>Entrer dans les champs ce que vous voulez modifier :</p>

       <form method = "get" action="">
            <input type="text" name="nvTitre" placeholder="nouveau titre">
            <input type="text"  name="nvDesc" placeholder="nouvelle description">
            <input type="date"  name="nvExp" placeholder="nouvelle date d'expiration">
            
             
            <input type="radio" name="status" value="privee" checked/>Privée
            <input type="radio" name="status" value="publique"/>Publique
            <input type = "submit" name = "modifListe" value = "Modifier">
       </form>
END;
        echo $html;
    }

    public function suppListe() {
        $html = <<<END
        <form method = "get" action="">
            <input type = "submit" name = "suppListe" value = "Supprimer la liste">
       </form>
END;
        echo $html;
    }
}