<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 09/01/2019
 * Time: 16:54
 */

namespace mywishlist\vues;

use mywishlist\utils\SlimSingleton;

class VueUtil
{
    public function __construct()
    {
        $this->app = SlimSingleton::getInstance();
        $this->url = $this->app->request->getRootUri();
    }

    public function connexion() {
        $html = <<<END
       <form method = "post" action="$this->url/connexion">
            <input type="text" name="login" placeholder="login">
            <input type="password"  name="pass" placeholder="pass">
            <input type = "submit" name = "connexion" value = "Connexion">
        </form>
END;
        echo $html;
    }

    public function inscription() {
        $html = <<<END
       <form method = "post" action="$this->url/inscription">
            <input type="text" name="login" placeholder="login">
            <input type="password"  name="pass" placeholder="pass">
            <input type = "submit" name = "inscription" value = "Inscription">
        </form>
END;
        echo $html;
    }

    public function modifier() {
        $html = <<<END
       <form method = "post" action="$this->url/modifierCompte">
            <input type="password" name="oldPass" placeholder="ancienMdp">
            <input type="password" name="newPass" placeholder="nouveauMdp">
            <input type = "submit" name = "modifPass" value = "Changer le mot de passe">
        </form>
END;
        echo $html;
    }

    public function champVide() {
        $html = <<<END
       <p>Veuillez remplir tous les champs</p>
END;
        echo $html;
    }

    public function pseudoInconnu() {
        $login = $_POST['login'];
        $html = <<<END
       <p>Erreur : le pseudo '$login' n'existe pas</p>
END;
        echo $html;
    }

    public function pseudoConnu() {
        $login = $_POST['login'];
        $html = <<<END
       <p>Erreur : le pseudo '$login' est déja utilisé</p>
END;
        echo $html;
    }

    public function nonConnecte() {
        $html = <<<END
       <h3>Vous devez vous identifier pour accéder aux listes de souhaits</h3>
END;
        echo $html;
    }

    public function fauxPass() {
        $html = <<<END
       <p>Erreur : pass incorrect</p>
END;
        echo $html;
    }

    public function compteCreee() {
        $login = $_POST['login'];
        $html = <<<END
       <p>Compte '$login' crée avec succés</p>
END;
        echo $html;
    }
}