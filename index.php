<?php

session_start();
require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = \mywishlist\utils\SlimSingleton::getInstance();

$app->get('/liste/afficher/:token', function ($token) {
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
        $controller = new \mywishlist\controls\AffController();
        $controller->affListe($token);
})->setName('affListe');

$app->get('/liste/modif/:token', function ($token) {
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    if(isset($_SESSION['login'])) {
        $controller = new \mywishlist\controls\AffController();
        $controller->modifListe($token);
    }
})->setName('modifListe');

$app->get('/item/afficher/:id/:token', function ($id) {
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
        $controller = new \mywishlist\controls\AffController();
        $controller->affItem($id);
})->setName('affItem');

$app->get('/item/modif/:id/:token', function ($id) {
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    if(isset($_SESSION['login'])) {
        $controller = new \mywishlist\controls\AffController();
        $controller->modifItem($id);
    }
})->setName('modifItem');

$app->post('/item/reserver/:id/:token', function($id, $token){
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    $controller = new \mywishlist\controls\AffController();
    $controller->reserverItem($id, $token);

})->name('reservItem');

$app->post('/message/new/:token' , function($token){
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    $controller = new \mywishlist\controls\AffController();
    $controller->nvMessage($token);
})->name('nvMessage');

$app->get('/', function(){
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    $controller->page();
})->name("index");

$app->post('/connexion' , function(){
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    $controller = new \mywishlist\controls\UtilController();
    $controller->connexion();
})->name('connexion');

$app->post('/inscription' , function(){
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    $controller = new \mywishlist\controls\UtilController();
    $controller->inscription();
})->name('inscription');

$app->post('/deconnexion' , function(){
    $controller = new \mywishlist\controls\UtilController();
    $controller->deconnexion();
})->name('deconnexion');

$app->post('/modifierCompte' , function(){
    $controller = new \mywishlist\controls\MainController();
    $controller->navBar();
    $controller = new \mywishlist\controls\UtilController();
    $controller->modifier();
})->name('modifierCompte');

$app->run();